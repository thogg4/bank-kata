const parser = require('./parser');

let configuration = () => {
  if (process.argv.length > 2)
    return { filename: process.argv.slice(2)[0] }
  else
    return { filename: './sequences' }
}
const config = configuration()

parser.parse(config.filename)
