const assert = require('assert');
const parser = require('./parser');

describe('parse', () => {
  it('should return the correct broken down sequences', () => {
    return parser.parse('./one_sequence').then((sequences) => {
      assert.deepEqual(sequences, [[
        ['   ', '  |', '  |'],
        [' _ ', ' _|', '|_ '],
        [' _ ', ' _|', ' _|'],
        ['   ', '|_|', '  |'],
        [' _ ', '|_ ', ' _|'],
        [' _ ', '|_ ', '|_|'],
        [' _ ', '  |', '  |'],
        [' _ ', '|_|', '|_|'],
        [' _ ', '|_|', ' _|']
      ]]);
    });
  });
});

describe('identifySequence', () => {
  it('should return the correct sequence', () => {
    assert.equal(parser.identifySequence(
      [[' _ ','| |','|_|'], ['   ', '  |', '  |'], [' _ ',' _|','|_ ']]
    ), '012 ERR')
  });

  it('should return the correct sequence with a unidentified character', () => {
    assert.equal(parser.identifySequence(
      [[' _ ','| |','|_|'], ['   ', '  |', '  |'], ['|| ','_  ','']]
    ), '01? ILL')
  });
});
