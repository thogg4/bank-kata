const fs = require('fs');
const readline = require('readline');

const parse = async (filename) => {
  const interface = readline.createInterface({ input: fs.createReadStream(filename), output: process.stdout, terminal: false })
  let sequences = []
  let currentSequence = []
  let lineCount = 1
  let filenameToWrite = 'numbers-' + Date.now()

  return new Promise((resolve) => {
    interface.on('line', (line) => {
      if (lineCount === 4) {
        // we are now at the end of the sequence
        let identified = identifySequence(currentSequence);
        fs.appendFileSync(filenameToWrite, identified + '\n', () => {});

        sequences.push(currentSequence);
        currentSequence = []
        lineCount = 1
      } else {
        // If we get here, but we have an empty line it's because we have a sequence full of 4's or 1's
        if (line.length === 0) { line = '                           ' }

        let chunked = line.match(/.{1,3}/g)

        chunked.forEach((chunk, index) => {
          if (currentSequence[index]) {
            currentSequence[index].push(chunk)
          } else {
            currentSequence[index] = []
            currentSequence[index].push(chunk)
          }
        });
        lineCount += 1
      }
    });

    interface.on('close', () => {
      resolve(sequences) 
    });
  });
};

const identifySequence = (sequence) => {
  // identify each number in the sequence, run it through the checksum and add the whole identified sequence to a file
  numbers = []
  sequence.forEach((numberGrid) => {
    joinedNumber = numberGrid.join('');

    if (joinedNumber === zero())
      number = 0;
    else if (joinedNumber === one())
      number = 1;
    else if (joinedNumber === two())
      number = 2;
    else if (joinedNumber === three())
      number = 3;
    else if (joinedNumber === four())
      number = 4;
    else if (joinedNumber === five())
      number = 5;
    else if (joinedNumber === six())
      number = 6;
    else if (joinedNumber === seven())
      number = 7;
    else if (joinedNumber === eight())
      number = 8;
    else if (joinedNumber === nine())
      number = 9;
    else
      number = '?';

    numbers.push(number);
  });
  
  if (numbers.some((item) => { return item === '?' }))
    status = 'ILL';
  else if (!checksum(numbers))
    status = 'ERR';
  else
    status = '';

  let stringToPrint = numbers.join('') + ' ' + status
  return stringToPrint
};

const checksum = (numbers) => {
  let sum = 0;
  numbers.reverse();
  numbers.forEach((number, index) => {
    sum += ((index + 1) * number)
  });
  numbers.reverse();

  if ((sum % 11) === 0)
    return true;
  else
    return false;
};

const zero = () => {
  return [' _ '] +
         ['| |'] +
         ['|_|'].join('')
};

const one = () => {
  return ['   '] +
         ['  |'] +
         ['  |'].join('')
};

const two = () => {
  return [' _ '] +
         [' _|'] +
         ['|_ '].join('')
};

const three = () => {
  return [' _ '] +
         [' _|'] +
         [' _|'].join('')
};

const four = () => {
  return ['   '] +
         ['|_|'] +
         ['  |'].join('')
};

const five = () => {
  return [' _ '] +
         ['|_ '] +
         [' _|'].join('')
};

const six = () => {
  return [' _ '] +
         ['|_ '] +
         ['|_|'].join('')
};

const seven = () => {
  return [' _ '] +
         ['  |'] +
         ['  |'].join('')
};

const eight = () => {
  return [' _ '] +
         ['|_|'] +
         ['|_|'].join('')
};

const nine = () => {
  return [' _ '] +
         ['|_|'] +
         [' _|'].join('')
};

exports.parse = parse;
exports.identifySequence = identifySequence;
