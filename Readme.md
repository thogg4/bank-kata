## Bank Kata

This exercise is based off of the requirements here: https://github.com/testdouble/contributing-tests/wiki/Bank-OCR-kata


### Running the parser

To run the file parser you need to have nodejs installed.

On a mac with homebrew:
```
brew install nodejs
```

Then you can use the runner and pass in the included sequences file:
```
nodejs runner.js sequences
```

The `sequences` file has example sequences of numbers that can be parsed.

When that is run a file will be created in the project directory and the parser will append the parsed and identified
sequences to it. The file that is created will have a name in this format: `numbers-[datetime string]`


### Running the tests

To run the test file you need the mocha library installed:
```
npm install -g mocha
```

You can then run the `mocha` command in this directory and it will run the tests in test.js
